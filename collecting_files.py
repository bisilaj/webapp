#!/usr/bin/env python

import re, os, shutil

# A quick and dirty script to go through all of these folders and move the files into another folder. 

__author__ = "Jon Bisila"


def move_files_to_folder(pathname = "./"):
	#First make a new directory to hold everything if you don't have one already.
	if not os.path.isdir("./Data"):
		os.makedirs("./Data")

	#We start by iterating through the folders and indexing into them one by one
	for folder in os.listdir(pathname):
		folder_name = str(folder)
		file_name = "pg" + folder_name + ".rdf"
		print("Filename: ", file_name)
		print("Folder name: ", folder_name)
		shutil.copy2("./" + folder_name + "/" + file_name, "./Data/")

def main():
	move_files_to_folder()
	return

if __name__ == '__main__':
	main()




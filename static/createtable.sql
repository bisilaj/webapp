DROP TABLE IF EXISTS gutenberg;
CREATE TABLE gutenberg (
  title text,
  author text,
  downloads real,
  language text,
  subject text,
  link text
);
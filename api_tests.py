import unittest
import api
from api import Column


class ApiTester(unittest.TestCase):
    """ Note that when refactoring our code to clean up redundant and extraneous code,
        we were able to eliminate about half of our tests. We initially thought we would
        need functions to do each of the queries one by one, but found that with the Flask
        functionality for parsing the URL for different query parameters we decided that
        we could use one function to handle the queries. Consequently, our test code looks
        a lot more sparse and actually only checks to see that our queries are in descending
        order of downloads, whether or not a given query returns items that all contain that
        query, and to make sure that with some inane query we don't return anything and fail
        gracefully."""
    """ Checks that the books are sorted in descending order by number of downloads """
    def test_books_sorted_by_popularity(self):
        books = api._local_get_books_by_popularity('Beowulf')
        if len(books) != 0:
            num_downloads = 9999999.0
            for book in books:
                self.assertTrue(num_downloads >= book[Column.DOWNLOADS.value])
                num_downloads = book[Column.DOWNLOADS.value]

    """ Checks that something in any section matches the query for each book """
    def test_get_books_by_popularity(self):
        books = api._local_get_books_by_popularity('Beowulf')
        contains_query = False
        for book in books:
            for value in book:
                if type(value) == float:
                    value = str(value)
                if value is None:
                    continue
                if 'beowulf' in value.lower():
                    contains_query = True
        self.assertTrue(contains_query)

    """ Using the function given as a parameter, queries something that does not exist
        Checks that the function responds properly """

    def helper_test_no_results_found(self, get_books_function):
        books = get_books_function('asdfghyahfigsjfvgh')
        self.assertTrue(books == [])

    def test_no_results_popularity(self):
        self.helper_test_no_results_found(api._local_get_books_by_popularity)


if __name__ == '__main__':
    unittest.main()
